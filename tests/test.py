import os
import sys
sys.path.append(os.path.dirname('../'))
from xpect import Xpect

if __name__ == '__main__':
    xpect = Xpect('ts', 10, 'http://localhost/')
    xpect.call('SessionConfigOpen', {'Timeout':10, 'Password':'3$rFcDe'})
    xpect.call('StatGetStatistics', [
        {
            'TsName': 'Test',
            'StatConfig': [
                {
                    'CreateSnapshot': True,
                    'TsInfoNeeded': True,
                    'BsInfoNeeded': True,
                    'SvcInfoNeeded': True,
                    'TableInfoNeeded': True,
                    'StatusInfoNeeded': True,
                    'RfInfoNeeded': True
                }
            ]
        }
    ])

