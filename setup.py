#!/usr/bin/env python
# -*- coding: utf-8 -*-

from setuptools import setup, find_packages

setup(
    name     = 'xpect-python',
    version  = '0.1.0',
    packages = find_packages(),
    requires = ['python (>= 2.5)'],
    description  = 'Dektek Xpect Mosaic API Module',
    long_description = open('README.markdown').read(),
    author       = 'Alexandr Mikhailenko a.k.a Alex M.A.K.',
    author_email = 'alex-m.a.k@yandex.kz',
    url          = 'https://bitbucket.org/enlab/xpect_python',
    download_url = 'https://bitbucket.org/enlab/xpect_python/get/master.tar.gz',
    license      = 'MIT License',
    keywords     = ['dektec', 'mosaic', 'dektec xpect mosaic', 'xpect', 'xpect soap', 'api', 'xpect api'],
    classifiers  = [
        'Environment :: Web Environment',
        'Intended Audience :: Developers',
        'Programming Language :: Python',
    ],
    install_requires=[
        'requests',
        'lxml'
    ],
)

