# Dektec Xpect Mosaic SOAP API Python Wrapper
![Alt text](https://www.python.org/static/opengraph-icon-200x200.png)
![Alt text](https://www.dektec.com/products/applications/Xpect/images/TopLeft.jpg)

24/7 TS monitoring and multiviewer

Xpect Mosaic is a software package that combines 24/7 transport-stream monitoring with a fully configurable multi viewer (Mosaic) of decoded video, audio bars and monitoring status.

###Dependencies
 - lxml
 - requests
   
###Install
	pip install lxml --user
    pip install requests --user
    git clone --depth=1 https://bitbucket.org/enlab/xpect_python xpect
   
```python
    class Xpect
      __init__(self, url=None, verbose=False, debug=False)
      call(method=None, params=None, session_id=None)
```

###Usage
```python
from xpect import Xpect

if __name__ == '__main__':
    xpect = Xpect('ts', 10, 'http://localhost/')
    xpect.call('SessionConfigOpen', {'Timeout':10, 'Password':'3$rFcDe'})
```


##### P.S. Test version. When I have time, working on it.

#### Please report error with the hashtag **#xpect_soap_api_python** to the mail <alex-m.a.k@yandex.kz>

