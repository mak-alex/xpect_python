# -*- encoding: utf-8 -*-
import requests
from lxml import etree
from lxml.etree import Element, SubElement
from lxml.etree import tostring as ts
from lxml.etree import fromstring as fs

# todo: т.к. железки/документации нет под рукой, не успел добавить
# сохранение идентификатора сессии в self.session_id (не забыть пофиксить)

# Тут все просто, оформляем наш SOAP
xsd_ns="http://www.w3.org/2001/XMLSchema"
xsi_ns="http://www.w3.org/2001/XMLSchema-instance"
soap_ns="http://schemas.xmlsoap.org/soap/envelope/"
encoding_style_ns="http://schemas.xmlsoap.org/soap/encoding/"
soap = '{{{}}}'.format(soap_ns)
NSMAP = {
    'soap': soap_ns, 'xsd': xsd_ns,
    'xsi': xsi_ns, 'encodingStyle': encoding_style_ns
}
''' Существующие методы у оборудования DekTec Xpect Mosaic
Списки у каждого ключа это существующие параметры, скармливать в том виде,
что описанны ниже...
'''
available_methods = [
    {'SessionOpen':[{'Timeout': None}]},
    {'SessionConfigOpen':[{'Timeout': None, 'Password': None}]},
    {'SessionClose':[[{}]]},
    {'StatCreateSnapshot':[{'TsName': None,}]},
    {'StatGetBasicStreamInfo':[{'TsName': None, 'Pids': None,}]},
    {'StatGetStatistics': [{ 'TsName': None, 'StatConfig': [
        {
            'CreateSnapshot': True,
            'TsInfoNeeded': True,
            'BsInfoNeeded': True,
            'SvcInfoNeeded': True,
            'TableInfoNeeded': True,
            'StatusInfoNeeded': True,
            'RfInfoNeeded': True
        }
    ]}]},
    {'StatGetStatistics2': [{ 'TsName': None }, { 'StatConfig': [
        {
            'CreateSnapshot': True,
            'TsInfoNeeded': True,
            'BsInfoNeeded': True,
            'SvcInfoNeeded': True,
            'TableInfoNeeded': True,
            'StatusInfoNeeded': True,
            'RfInfoNeeded': True
        }
    ]}]},
    {'StatGetPids':[{'TsName': None,}]},
    {'StatGetServiceIds':[{'TsName': None}]},
    {'StatGetServiceInfo':[{'TsName': None, 'ServiceIds': None,}]},
    {'StatGetTableIds':[{'TsName': None}]},
    {'StatGetTableInfo':[{'TsName': None, 'TableIds': None}]},
    {'StatGetTemplateRuleIds':[{'TsName': None}]},
    {'StatGetTsInfo':[{'TsName': None}]},
    {'StatLogGetEntriesByTime':[{
        'TsName': None,
        'StatLogType': None,
        'TimeStart': None,
        'TimeEnd': None,
    }]},
    {'CaptTrigger':[{'TsName': None}]},
    {'CaptArmTrigger':[{'TsName': None, 'Arm': None}]},
    {'CaptGetTriggerCondition':[{'TsName': None}]},
    {'CaptSetTriggerCondition':[{'TsName': None, 'TriggerEv': None}]},
    {'CaptDeleteFile':[{'TsName': None, 'TsUrl': None}]},
    {'CaptGetInfo':[{'TsName': None}]},
    {'CaptGetState':[{'TsName': None}]},
    {'StatGetMonStatus':[{'TsName': None, 'StatIdT': None}]},
    {'StatGetOverallStatus':[{'TsName': None}]},
    {'ConfGetAvailableMosaicScreens':[{}]},
    {'ConfGetAvailablePorts':[{}]},
    {'ConfGetMonitoringState':[{'TsName': None}]},
    {'ConfGetTsConfig':[{}]},
    {'ConfGetTsNames':[{}]},
    {'ConfGetTsNamesPortInfo':[{}]},
    {'ConfGetSettingsIds':[{'TsName': None}]},
    {'ConfGetSettings':[{'TsName': None, 'Type': None, 'SettingsId': None}]},
    {'ConfSetSettingsAttribute':[{
        'TsName': None,
        'Type': None,
        'Settings': None
    }]},
    {'ConfSetConfigPassword':[{'Password': None}]},
    {'ConfSetIpForwPars':[{'TsName': None, 'IpForwPars': None}]},
    {'ConfSetMonitoringState':[{'TsName': None, 'MonState': None}]},
    {'ConfSetSettings':[{'TsName': None, 'Type': None, 'Settings': None}]},
    {'ConfSetSettingsId':[{'TsName': None, 'Type': None, 'SettingsId': None}]},
    {'EvCreateComposite':[{
        'TsName': None,
        'CompEventId': None,
        'EventIds': None
    }]},
    {'EvDeleteComposite':[{'TsName': None, 'CompEventId': None}]},
    {'EvGetStringResources':[{}]},
    {'EvLogGetActiveEntries':[{'TsName': None}]},
    {'EvListComposite':[{'TsName': None}]},
    {'EvLogGetEntriesByIndex':[{
        'TsName': None,
        'EvType': None,
        'EvFilter': None,
        'IndexFirst': None,
        'IndexLast': None
    }]},
    {'EvLogGetEntriesBySeqNr':[{
        'TsName': None,
        'EvType': None,
        'EvFilter': None,
        'SeqNrFirst': None,
        'SeqNrLast': None
    }]},
    {'EvLogGetEntriesByTime':[{
        'TsName': None,
        'EvType': None,
        'EvFilter': None,
        'TimeStart': None,
        'TimeEnd': None
    }]},
    {'EvLogGetEntryRange':[{
        'TsName': None,
        'EvType': None,
        'EvFilter': None
    }]},
    {'EvLogGetEntryRangeByTime':[{
        'TsName': None,
        'EvType': None,
        'EvFilter': None,
        'TimeStart': None,
        'TimeEnd': None,
        'MaxEntries': None
    }]},
    {'EvUnsubscribe':[{'TsName': None, 'SubscrIds': None}]},
    {'EvSubscribe':[{'TsName': None, 'IpAddr': None, 'Eventids': None}]},
    {'PreviewSelectService':[{'TsName': None, 'SvcID': None}]},
    {'GlobalGetVersion':[{}]},
    {'GlobalGetServerInfo':[{}]},
    {'GlobalGetLastError':[{}]},
    {'SvcGetState':[{}]}
]

def chk_method(n, p=None):
    '''chk_method
    Данный метод проверяет, существование метода,
    передоваемого в качестве аргумента методу call

    Возвращает boolean
    '''
    if isinstance(available_methods, list):
        for m in available_methods:
            if isinstance(m, dict):
                if n in m:
                    return True
        return False

class Xpect:
    '''DekTec Xpect Mosaic
    Xpect Mosaic is a software package that combines 24/7
    transport-stream monitoring with a fully configurable multi viewer (Mosaic)
    of decoded video, audio bars and monitoring status.

    url - string
    verbose - bool
    debug - bool

    Usage:
        from xpect import Xpect

        if __name__ == '__main__':
            xpect = Xpect('ts', 10, 'http://localhost/')
            xpect.call('SessionConfigOpen', {'Timeout':10, 'Password':'3$rFe'})
    '''
    def __init__(self, url=None, verbose=False, debug=False):
        self.url = url
        self.verbose = verbose
        self.debug = debug
        self.session_id = None

    def call(self, method=None, params=None, session_id=None):
        ''' Отправка сообщени на устройство Dektec Xpect Mosaic
        Здесь тоже все по-дефолту, клепаем наше тело SOAP и отправляем,
        получаем ответ и делаем с ним то, что душе угодно

        method - здесь указываем имя методы в Dektec Xpect Mosaic (string)
          все методы описанны в списке available_methods
        params - здесь же передаем список со словарями (list)
          все параметры для каждого из методов указаны в списке available_methods

        Возвращает SOAP
        '''
        if not chk_method(method):
            print('This wrong method')
            return

        self.Envelope = Element(soap + 'Envelope', nsmap=NSMAP)
        Body = SubElement(self.Envelope, soap + 'Body', nsmap=NSMAP)
        El = SubElement(Body, method, nsmap=NSMAP)
        El.attrib['xmlns'] = 'urn:xpect'

        # если передали идентификатор сессии или был получен
        # добавляем в наш SOAP, в противном случае ничего не делаем
        if session_id or self.session_id:
            SubElement(El, 'SessionId', nsmap=NSMAP)\
                .text = str(session_id) if session_id else str(self.session_id)

        def parse_data(params, d=None):
            ''' Разбор полетов
            Добавляем наши параметры в SOAP
            '''
            if isinstance(params, list):
                for i in params:
                    if isinstance(i, dict):
                        for k, v in i.items():
                            if isinstance(v, list):
                                parse_data(v, SubElement(El, k, nsmap=NSMAP))

                            elif d is None:
                                SubElement(El, k, nsmap=NSMAP).text = str(v)
                            else:
                                SubElement(d, k, nsmap=NSMAP).text = str(v)
                    else:
                        pass
            elif isinstance(params, dict):
                for k, v in params.items():
                    if isinstance(v, dict):
                        parse_data(v, SubElement(El, k, nsmap=NSMAP))
                    elif d is None:
                        SubElement(El, k, nsmap=NSMAP).text = str(v)
                    else:
                        SubElement(d, k, nsmap=NSMAP).text = str(v)
            else:
                print('unknown error')

        parse_data(params)

        try:
            self.response = self.__request__()
        except:
            # Если не получилось получить ответ,
            # значит отсутствует идентификатор и таковой не получилось получить
            # в ходе работы, т.е. просто выводим в stdout на сформированный SOAP
            if not session_id and not self.session_id:
                print('Do not specify a session ID, '
                    'further work is possible in demo mode')
                print(ts(self.Envelope, pretty_print=True))

    def __request__(self,):
        ''' Таки шлем
        Шлем наше тело SOAP, на указанный юрл, получаем ответ и отдаем пользователю

        Возвращает SOAP
        '''
        enc_request = ts(self.Envelope)
        headers = {
            "Content-Type": "text/xml; charset=UTF-8",
            "Content-Length": str(len(encoded_request)),
            "SOAPAction": ""
        }

        if self.verbose: print(ts(self.Envelope, pretty_print=True))
        resp = requests.post(url=self.url, headers=headers, data=enc_request)

        if resp.text:
            response = re.findall(
                '(<soap:Envelope.*?</soap:Envelope>)', resp.text
            )[0]

            if self.verbose:
                print(ts(fs(response), encoding='UTF-8', pretty_print=True))

            return response


if __name__ == '__main__':
    xpect = Xpect('http://localhost/')
    xpect.call('SessionConfigOpen', {'Timeout':10, 'Password':'3$rFcDe'})
    xpect.call('StatGetStatistics', [
        {
            'TsName': 'Test',
            'StatConfig': [
                {
                    'CreateSnapshot': True,
                    'TsInfoNeeded': True,
                    'BsInfoNeeded': True,
                    'SvcInfoNeeded': True,
                    'TableInfoNeeded': True,
                    'StatusInfoNeeded': True,
                    'RfInfoNeeded': True
                }
            ]
        }
    ])
